# Retour d’Expérience sur une UE Projet en Licence Informatique
C'est on fait un retour d'expérience sur une vieux projet qui est un peu mieux pilote à l'université de Bordeaux en licence informatique.
## Contexte
 Alors un petit peu de contexte.
C'est une UE six crédits, ce qui se déroule sur toute l'année, ça fait 40 heures de présentiel sur l'année donc c'est 1 h 20 par semaine.
Donc c'est un temps très court chaque semaine, ce qui implique beaucoup de travail à la maison.
On a un effectif assez fort environ 250 étudiants en licence informatique à gérer.
On se retrouve avec 12 groupes de TD et une dizaine de chargés de TD qui vont se répartir ces groupes.
Dans chaque groupe de TD, les étudiants se répartissent en équipes de 3.
On aime bien l'idée d'une équipe.
On a donc environ 75 équipes à gérer.

 Cette UE se trouve en 2ième année de licence informatique.
Elle a quelques prérequis en initiation programmation C, un peu de programmation web également et elle a une autre UE en L3 qui suit derrière sur de projets de développement logiciel.

## Objectifs pédagogiques

L'objectif pédagogique principal est de former nos étudiants à des méthodes et des bonnes pratiques et également à des outils techniques pour qu'ils puissen participer au développement de gros logiciel.
Cela se décline donc une liste d'objectifs plus précis.
On veut renforcer leur niveau en programmation, faire un petit peu d'algorithmique, leur apprendre à travailler en équipe, à pouvoir tester du code, donc faire des tests unitaires, vérifier la couverture des tests (avec l'outil gcovb).
On veut également faire un peu d'initiation au génie logiciel.
Pour cela on veut les sensibiliser à l'importance d'avoir code propre, du code bien documenté, du code maintenable, un peu de découpage modulaire et un peu d'interopérabilité.
On veut également qu'ils comprennent l'importance de respecter une spécification.
On veut également leur apprendre les techniques de débogage, maitriser un IDE (dans notre cas VS Code).
De manière plus terre à terre on veut également qu'ils maitrise la chaine de compilation (via les outils make puis cmake).

Et puis comme ils travaillent à plusieurs, on veut également qu'ils utilisent git et puis de manière un peu plus avancée Gitlab.
Et puis on veut qu'ils puissent également s'approprier une bibliothèque et la documentation plus ou moins en autonomie.

Et puis on termine avec des technologies un peu plus un peu plus récentes pour  finaliser le projet.

## Pourquoi un projet sur toute l'année ?

Alors historiquement c'était une UE qui avait à peu près le même contour et qui avait des modalités un peu plus classiques, des cours et des TD où on présentait toutes ces technos et ces méthodes, et puis en fin de semestre puisque c'était sur un semestre, il y avait des mini projets pour mise en application de tout ça.
Et puis on avait remarqué une adhésion modérée des étudiants aux mini projets qui avaient une interrogation, une incompréhension tout à fait légitime qui disait : "moi quand je code ma petite bidouille, je fais du quick and dirty et ça marche très très bien comme ça.
Je n'ai pas besoin d'apprendre tous vos outils.
Je fais gcc *.C et ça marche bien.
Pour débogger, pas besoin de gdb.
Je rajoute des printf de temps en temps, ça suffit !"

Ces remarques sont tout à fait légitimes.
Pour des tous petits programme, c'est une méthode très efficace de développment.

Et donc on avait du mal à les convaincre que d'accepter a perdu un peu de temps à apprendre ces technologies pour au final en gagner beaucoup mais beaucoup après.
Et donc c'est pour ça qu'on a migré sur une UE projet sur tout au long de l'année.
Le fait que le projet se déroule sur toute l'année nous permet d'atteindre une taille critique qui permet de mettre en évidence l'intérêt de ces outils et de ces méthodes de développement.

Au fil des années nous avons convergé vers un scénario qui se veut motivant pour les étudiants et qui permet d'introduire les outils et méthodes progressivement. A chaque fois on introduit les outils et méthodes quand cela devient utile pour l'avancement du projet. Lorsque le projet avance la maitrise des outils n'est plus simplement utile elle est nécessaire du fait de la montée en complexité du projet. Ici ile timing est assez difficile à trouver. Si on avance trop vite les étudiants n'ont pas le temps d'accéquérir les compétences associées et ils s'enlisent dans le projet. Chaque année nous ajustons le timing en fonction de la difficulté intrinsèque du projet mais aussi de l'avancement de la promotion.

%% peut-être pas à sa place
Pour mettre tout ça en place, on a importé progressivement tout ça sous Moodle en nous appuyant intensivement sur le plugin VPL quie permet une correction automatique de la plupart des rendus.

%% peut-être pas à sa place
Nous avons également basculer sur l'utilisation de Gitlab pour gestion de projet à la fois côté étudiants et enseignants. Nous reviendrons sur ce point là un peu plus lon.

Le projet à réalisé est en fait un prétexte (un fil rouge) pour introduire les outils et méthodes de développement. Il s'agit d'un petit jeu de logique qui change chaque année. L'important est que le projet soit suffisamment simple pour que les étudiants puissent le réaliser en autonomie mais suffisamment complexe pour qu'ils aient besoin d'utiliser les outils et méthodes de développement.

Nous avons choisi comme thématique un petit jeu mais ça pourrait être tout autre chose, s'il est possible de faire varier le sujet d'année en année tout en gardant la même structure. Dans notre contexte, il est important que le sujet du projet soit différent chaque année et soit le même pour toutes les équipes.


### Le scénario pédagogique

Alors le scénario qu'on a développé est le suivant.
Donc au début on arrive avec le noyau déjà implémenté sous forme d'une bibliothèque avec une API documentée qu'on leur propose.
Et le première étape, c'est juste à implémenter le jeu en mode texte.
Donc ça démarre un petit peu doucement.
Du coup ça permet de de se remettre un peu les mains dans le C puis d'avoir à gérer la compilation un peu un peu plus avancée.
Puis quand ils ont fait ça, on leur demande d'écrire des tests pour la bibliothèque donc au début et donc c'est la première fois qu'ils ont à développer des tests, c'est ils sont un peu frileux là dessus.
Du coup pour les motiver, on leur donne des versions beugué de notre bibliothèque.
Il faut qu'ils essayent de trouver les bugs de nos bibliothèques.
Alors comme on a un peu d'expérience, les bugs qu'on fait c'est à peu près ceux qu'on retrouve régulièrement sur les les projets précédents.
Donc c'est si arrivent à trouver nos bugs, ben ils vont probablement trouver les bugs qu'eux vont faire un petit peu par la suite.
On dit qu'il faut qu'il fasse du code maintenable et on voudrait qu'il le centre dans leur chair.
Qu'est ce qu'on fait? On les a prévenus au début, mais là on le met en application.
Up Pardon, j'ai oublié une étape.
Une fois que tout ça est mis en place, hop on enlève la bibliothèque et c'est eux d'implémenter le noyau.

Ils choisissent leur structure donnée.
L'API ne bouge pas et ils implémentent cela et peuvent s'appuyer sur leurs tests.
Donc là ils commencent à dire ah finalement ce n'était pas si mal d'avoir d'avoir d'avoir des tests.
On leur demande de faire du code maintenable, on leur explique pourquoi c'est utile et puis on leur a dit depuis le début attention, le cahier des charges va changer, le code doit s'adapter Et là du coup là, c'est ce qu'on appelle le caprice du client.
On modifie un peu l'API, on étend un peu les règles du jeu, on fait une petite variante et là ils on va faire un peu de refactoring, c'est à dire s'adapter au changement.
Là ils commencent à dire ah, ils ne disent pas que des bêtises, peut être, peut être c'est utile.
Mais quand ils ont implémenté ça proprement.
Après, il y a une phase de relecture de code où chaque chaque étudiant doit récupérer le code d'un autre groupe et le lire, l'évaluer, le tester et puis vérifier l'interopérabilité entre son code et le sien en faisant travailler son noyau avec l'interface texte et les tests de l'autre groupe et inversement.
Donc là, ils sont confrontés à du code d'autres étudiants et se dit ah tiens, j'avais fait des choix qui me paraissait évident mais finalement il y a d'autres choix qui ne sont pas plus ridicules et c'est intéressant.
Et puis des fois c'est du code moche et là ils voyent qu'est ce que c'est que lire du code moche? Donc ça c'est très formateur.
Donc nous derrière on doit évaluer, il évalue le code et après nous on évalue le code et on évalue les évaluations.
C'est quelque chose assez chronophage mais qui est assez payant.
On arrive après Hugo au semestre d'après et du coup là dessus on commence à développer des choses un peu plus sophistiquées où là on commence à accélérer les choses.
Alors comme certains étudiants, on commence à être en difficulté et du coup se traîne des bugs qui traînent dans leur bibliothèque.
On leur fournit une version corrigée de la bibliothèque, comme ça ils peuvent repartir sur des bases saines et là on leur demande bien des trucs un peu plus algorithmique, comme un solveur qui essaie de résoudre le casse tête.
Là de deux niveaux d'exigence un niveau standard implémenté, un bruit de force, ça suffit.
Donc pour ceux qui sont un peu un peu en difficulté, on leur demande pas d'aller plus loin.
Et puis au contraire pour les groupes qui sont forts et motivés.
Non mais si vous voulez, vous pouvez optimiser votre code, faire des choses sophistiquées Et puis on propose du ranking automatique sur les sur les performances des différents solveur et du coup ça sa stimule la tête de promo.
Ça c'est agréable.
Et d'ailleurs souvent ils vont plus vite que nous sur sur les implementation.
A partir de là, on continue à enchaîner des bobs, à rajouter une interface graphique.
Voici la documentation de la SDL.
Voici.
Voici un prototype pour jouer les principales fonctions.
Si vous voulez faire du quelque chose de minimal, vous avez toutes les.
On vous a donné un petit démo avec les fonctions nécessaires.
Si vous voulez aller plus loin, allez y, débrouillez vous.
Et puis à la fin pour qu'il puisse repartir avec quelque chose de démontrable chez eux, à la maison et cetera.
On rajoute une petite couche soit web sur Android selon les années, soit les deux.
Comme ça ils ont un produit fini qu'ils peuvent, avec lequel ils peuvent jouer et qu'ils peuvent montrer à leur famille.
Ben voilà le déroulé sur l'année.
Donc voyez, c'est une quinzaine de TD qui généralement dure à peu près deux semaines et à chaque fois on a une évaluation et on mélange évaluation individuelles et par équipe.
Allez y et les heu pour pour éviter un peu, pour qu'éviter qu'il y en ait qui se laisse porter par le groupe et ne travaille pas assez les feuilles de TD ou les acquis.
Et puis il y en a essentiellement des évaluations automatiques avec quelques évaluations manuelles surtout sur la fin.
Donc on se retrouve avec plus d'une trentaine de notes.
Sur l'année.
Je repasse la main à Aurélien.
Merci Nicolas.
Donc là je vous présente par exemple le zoom sur le TD numéro cinq, donc le TD sur les tests.
Donc là l'objectif pédagogique c'est d'implémenter les tests.
Donc c'est plutôt au début de l'année TD5.
Et donc ça c'est la une, une feuille de TD type sur Moodle.
Donc Moodle, j'imagine que tout le monde connaît la plateforme pédagogique qu'on utilise nous à Bordeaux, mais un peu partout en France aussi.
Et donc voilà, donc il y a des informations de base sur sur la durée du TD, Donc c'est là, ça sera sur deux semaines.
Et puis il y a des informations sur le fait qu'il y aura deux rendus, le coefficient.
Donc tout ça est annoncé à l'avance.
Et vous noterez que le premier rendu a un rendu initial, il est individuel, une seule passe.
Donc j'expliquerai après.
Est ce que ça veut dire cette notion de passe et ensuite le rendu final pour coefficient cinq un peu donc le vrai travail en équipe de passe.
Alors on fournit en général un certain nombre de ressources préliminaires, vidéos ou supports de cours, etc.
Qui doivent pouvoir travailler à la maison à l'avance.
On leur dit regardez la vidéo, comprenez là.
Et puis ensuite il y a la feuille de le sujet du TD, Donc là c'est du markdown compilé en guide de la page.
Donc je vais dire un mot tout à l'heure, ça c'est très très très.
C'est leur feuilles de TD, ils ont une activité préliminaire puis des rendus à fournir à la fin il y a un petit sondage pour qu'ils nous fassent des retours.
Combien de temps ça vous a pris? Est ce que vous trouvez que c'était dur ou pas dur? Donc on essaie de là.
C'est la première fois qu'on fait ça cette année là.
Donc là, les rendus qu'il y a ici, en fait, ce sont des rendus dits PPL, Donc PPL c'est une activité particulièrement dans Moodle qui permet justement de mettre en place des corrections automatiques.
Je vais je vais expliquer ça après.
Donc le déroulement type d'une séance de 1 h 20, donc c'est des séances de 1 h 20, c'est très court finalement une fois par semaine c'est vraiment de la coordination synchronisation motivation des équipes.
Donc le chargé de TD gère à peu près cinq cinq équipes de trois étudiants, vous voyez? Et donc on fait plus ou moins on fait débriefe sur le sur le dernier rendu.
En général ils ont des doléances à nous faire remonter, je comprends pas.
Et donc on débriefe, on discute, on débriefe également l'activité préliminaire ou on lance l'activité préliminaire.
Quant à ce fait, en séance de TD, on coordonne les équipes et on les incite à se répartir le travail pour les semaines qui suivent.
Donc on a un échéancier comme ça donc, et on recommence comme ça chaque semaine.
Alors un petit mot sur notre stratégie d'évaluation.
Donc j'ai mis entre parenthèses automatique parce qu'ici ce n'est pas vraiment important.
Donc vous avez compris le TD découpé en deux activités une activité préliminaire avec un.
Donc ça je l'avais déjà dit, un rendu initial plutôt individuel à préparer, puis une activité principale qui elle est vraiment une activité sur le projet avec un rendu sur le projet et plutôt en équipe.
Donc ici ça se découpe comme ça pour le T des cinq et les principes de l'évaluation qu'on a mis en place fonctionne en deux passes.
Donc ça on ne l'a pas fait ou on ne l'a pas imaginé du premier coup.
Donc j'en dis un mot maintenant.
Donc on a une première passe qui est à l'aveugle ou presque.
Donc dans l'activité VBL que je vais détailler ici vont nous rendre quelque chose et on va en fait avant la deadline leur permettre de lancer une sorte de script d'évaluation mais qui va donner un feedback, un retour partiel.
On ne veut pas qu'ils aient toute la solu fin dans toute l'évaluation complète sur ce qu'ils ont fait.
Ils peuvent le lancer autant de fois qu'ils veulent.
L'intérêt qu'il y a pour nous, c'est que ça évite les faux départs, les tout ce qui est les étudiants qui.
Qui comprennent qui? Moi je dis ça a marché chez moi mais j'ai eu zéro.
Qu'est ce qui s'est passé? Ben ouais, ils n'ont pas appelé le fichier du bon nom, il y avait un cahier des charges, ils l'ont pas respecté, etc.
Donc là ça a permis vraiment d'éviter cette espèce d'erreur où ils sont très frustrés en général de se prendre zéro parce que puis nous on est un peu gênés aussi parfois de leur mettre zéro.
Donc ça, voilà, ils ont là, ils ont plus d'excuses parce qu'ils pouvaient le tester.
Donc nous ça nous a fait gagner beaucoup d'énergie, ça leur donne de la rigueur et eux ça leur apprend la rigueur.
Merci.
Et du coup, au moment de la deadline, on va cette fois ci nous, de manière offline, lancer l'évaluation de tous les projets.
Donc là ils peuvent plus rien modifier, c'est figé, ils ont fait leurs premiers rendus donc ça c'est la fin de la première passe et ils ont ils obtiennent les commentaires complets sur leur travail et la note, la note de la première passe.
A partir de ce moment là, on veut être on va débloquer une deuxième passe qui va se dérouler en général à cheval sur la prochaine séance.
Donc c'est un travail qui peut faire en plus et c'est le deuxième pas.
Ça vise à les aider à se rattraper ou à se perfectionner.
Ils n'ont pas eu 100 % des points à la première passe.
On leur donne la possibilité en fait de rejouer avec cette deuxième passe.
Ça c'est motivant pour eux parce que des fois ils ont envie d'aller chercher les 100 % des points.
Mais on veut, on veut quand même vérifier qui, voilà qui.
Donc du coup, note finale ça sera le max première passe et moyenne des deux passes.
On privilégie quand même la première passe.
Oui.
Du coup c'est quelque chose d'important parce qu'en fait les compétences s'empilent tout au long du projet, donc on ne veut pas qu'il y ait trop de lacunes et qu'il ait la possibilité de se perfectionner même s'ils ont raté l'évaluation qui puisse continuer à travailler pour acquérir au moins le minimum de compétences pour chaque brique pour avancer.
Et j'avais mis feedback partiel en rouge parce que je voulais dire un truc important le feedback partiel.
En fait, effectivement il est important on pourrait dire autant leur donner le feedback comblé dès le départ, mais en fait ce n'est pas une bonne idée.
On a expérimenté ça, ils font du je ne sais pas comment on dit ça à du code aux essais erreurs.
Vous voyez ce que je veux dire? Il faut un if dans un sens, puis si ça ne marche pas, il l'essaye dans l'autre sens, ça a plein de biais et on veut qu'ils réfléchissent quand même.
Donc c'est pour ça que le projet quand même suffisamment important pour qu'ils pour qu'ils ne fassent pas ce genre de stratégie là qui nous intéresse pas du tout.
Alors là c'est maintenant.
C'est la partie un peu plus technique donc pour implémenter en fait cette stratégie d'évaluation, on a utilisé en fait l'activité VPS dans Moodle.
Donc ça c'est la mise en œuvre avec cet outil là.
Et donc en fait, vous voyez, ça se présente comme ça, l'activité VPS.
Les étudiants vont nous rendre ne vont pas coder dans GPL en fait, comme on pourrait le faire, ils vont simplement nous rendre l'URL d'un dépôt git et un hash de commit.
C'est tout ce qui nous rendent.
Et donc j'expliquerai juste après.
Et donc ils ont deux boutons essentiellement qui les intéressent le bouton run et le bouton est Val qui sont la petite fusée ici et là.
Et en fait, à partir de ce moment là sur le run, c'est là qu'on implémente le feedback partiel.
Donc on peut bloquer, verrouiller ces boutons ou les déverrouiller en fonction de la phase si on est en première ou deuxième passe et quand on appuie sur quand ils peuvent appuyer sur le bouton eval par exemple pour le deuxième passe, et bé ça leur débloque ce que vous voyez à droite les.
Les commentaires complets, donc le feedback complet en fait avec la note donc ça fonctionne comme ça et de détails.
Oui donc effectivement ici je ne peux pas rentrer le détail et expliquer le détail, mais vous voyez en fait en gros on pour leurs projets, on le teste, on le compile, etc.
On passe des tests à nous et puis à la fin, en fonction du test nombre de tests réussis, ils ont une certaine note D'accord.
Donc là il fallait tester les tests.
Alors le backend technique.
J'ai deux slides pour présenter ça.
Le premier, le premier aspect c'est sur le PPL donc j'ai déjà dit un petit peu l'essentiel.
Les étudiants en fait ne code pas dans Moodle, il code dans Gitlab leur projet en C et puis il le soumettent le comité à l'activité PPL.
Et puis cette PPL elle est couplée à un serveur d'exécution avec une infrastructure infrastructure un peu particulière qu'on a mis en place à Bordeaux avec un système de Docker pour contrôler l'environnement d'exécution qui soit vraiment similaire aux conditions de travail en TP.
Et donc avec tout ça, en gros, si vous voulez, les scripts qu'on programme vont passer un certain nombre de tests.
Donc là on a à peu près 5000 lignes de script shell qu'on a codé, donc c'est un peu fastidieux.
Et ce script shell en gros, il fait quoi? Il va clonent le dépôt étudiant, il va clones, il va récupérer dans un autre dépôt nos script shell.
Il fait une tambouille et remonte aux étudiants.
Le feedback, les commentaires et la note.
Donc voilà, c'est un système qui fonctionne comme ça.
Alors chose rigolote, c'est que si on prend la solution enseignant, on peut la jouer dans le logiciel et puis on vérifie qu'on a 100 % des points.
Voyez, donc on fait un peu de l'intégration continue sur, on va dire, nos propres tests.
Deuxième aspect technique dont je voulais parler, qui a un petit peu transverse, c'est le côté écrire et publier son cours avec Gitlab.
C'est donc là ça concerne plus l'aspect enseignant.
On est une équipe pédagogique.
J'ai dit tout à l'heure d'une dizaine d'enseignants alors pas tous permanents, donc on les implique pas tous au même niveau évidemment.
Mais l'idée c'est que comment on va gérer nos supports, comment on va gérer nos scribes.
Donc on utilise évidemment des dépôts guides pour partager toutes ces ressources, les modifier à plusieurs mains.
Et donc on a du coup la possibilité d'avoir un travail collaboratif de l'équipe pédagogique sur l'ensemble des supports.
Et de fil en aiguille, les choix qu'on a choisis, c'était vraiment d'externaliser au plus possible les ressources Hors de Moodle, on ne veut pas avoir à faire des glisser coller de documents PDF dans Moodle, ce genre de choses.
C'est trop fastidieux à maintenir Ce qu'on veut nous c'est.
Je me fixe une typo sur un sujet qui est écrit en markdown et ensuite j'ai une chaîne d'intégration continue branchée dans le framework Gitlab qui va automatiquement compiler avec les outils qui vont bien.
Donc ici par exemple emc doc, un sujet en HTML, le publier automatiquement sur des guides de la page et la page Moodle sera automatiquement à jour puisqu'en fait elle ne contient qu'un lien sur ce site de ressources externalisées.
L'avantage qu'il y a à faire ça aussi c'est qu'on moodle, ça a tendance à refermer les cours avec des accès authentifiés.
Là on peut avoir une vraie, un vrai.
C'est un vrai site de ressource qu'on peut repartager avec la communauté comme comme autrefois, comme jadis.
Donc pour conclure, pour conclure, donc, c'est cette approche, on la trouve très motivante pour les étudiants, on a des très bons retours.
C'est NYU qui demande beaucoup de travail et aux étudiants on leur annonce dès le début Attention, c'est difficile, il va falloir travailler, Ils le comprennent et c'est également eux qui demandent beaucoup de travail pour les enseignants et notamment pour le responsable qui chapeaute tout ça.
Mais c'est également une avenue qui apporte beaucoup de satisfaction.
Donc pour les étudiants, à la fin, quand ils se retournent à la fin de l'année qui voyaient le travail accompli, les compétences acquises, là, ils sont ils sont fiers d'eux, ils ont bien transpiré, mais ils sont fiers d'eux.
Et puis pour l'équipe enseignante, c'est agréable puisqu'on a quand même des retours positifs, des très positifs, des étudiants qui plébiscitent cette ces modalité là.
Donc on a dépensé beaucoup d'énergie pour mettre en place ces systèmes d'évaluation automatiques qui permettent de gérer des grosses cohortes.
Et donc là aussi pour les supports pour gérer une grosse équipe pédagogique.
Malgré tout, il y a des difficultés intrinsèques entre guillemets.
Donc l'écriture de ces scripts et leur maintien, c'est quelque chose qui reste chronophage.
Les retours des scripts ne sont pas les retours faits aux étudiants.
C'est difficile d'être exhaustif et clair, donc il y a toujours des ambiguïtés, des flous qui restent et donc pas toujours explicites ni pour les gens, ni pour les étudiants et parfois pas assez pour les chargés de TD.
Donc là il y a encore des pistes d'amélioration et du coup c'est difficile d'avoir aussi des scripts qui envisagent tous les cas de figure.
On est toujours réactifs et c'est pour ça que toute cette chaîne d'intégration continue a été mise en place.
C'est pour pouvoir passer moins de temps à faire vivre ce cours.
Il y a aussi des bénéfices cachés qui est du coup la satisfaction de faire quelque chose de plus rigolo qui est d'écrire du code pour évaluer les codes plutôt que de lire n fois les mêmes codes avec les mêmes erreurs.
Et puis il y a également une changement, un changement de posture A en TD.
Du coup comme c'est le script, le méchant qui fait l'évaluation, les qui envoient les mauvaises notes.
Nous on est du côté des gentils qui aidons les étudiants à affronter ce script pour que finalement ils puissent avoir les bonnes notes.
