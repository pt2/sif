# Exposé SIF : Les Projets en Licence

* *Le 10 mai 2023 à Paris, au centre Malher dans le 4e arrondissement (Université Paris 1 Panthéon-Sorbonne).*
* Apprentissage par projets : des évidences aux réalités. Pédagogie, évaluation, impact.
* Site Web : [Journée Enseignement 2023](https://www.societe-informatique-de-france.fr/les-journees-sif/les-journees-pedagogiques/journee-enseignement-2023/)
* Retour d'expérience sur une UE Projet en Licence Informatique à l'Université de Bordeaux.
* Auteurs : Nicolas Bonichon & Aurélien Esnard
