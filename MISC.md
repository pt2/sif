# MISC

## Pédagogie de Projet : késako ?

D'après [Wikipedia](https://fr.wikipedia.org/wiki/P%C3%A9dagogie_de_projet) :

* objectif de réaliser une production **concrète**
* intégrer des savoirs nouveaux
* projet pouvant être individuel ou collectif
* travail **coopératif** en petit groupe : division du travail, rotation dans les rôles assumés par chacun
* une **pédagogie active** : rendre l'étudiant acteur de ses apprentissages
* placer les étudiants en situation de **résolution de problèmes**
* pédagogie fondée sur la **motivation** des étudiants à réaliser une production concrète

Les étapes du projet :

* préciser les objectifs du projet
* **choisir** : choix d'un thème intéressant, vérifier la faisabilité, ... -> choix libre comme une étape à part entière du projet ou imposé par l'équipe pédagogique (méthode traditionnelle)
* **produire** : réalisation du projet de façon **autonome**
* **auto-évaluation** des étudiants par rapport aux objectifs de départ
* **le bilan** : une étape à part entière dans le projet avec la présentation du travail réalisé à ses pairs (ou plus largement) -> restitution et valorisation du travail accompli

**Intérêts** : *les étudiants font leurs propres choix et suivent des stratégies différentes...*

* richesse de la différenciation des itinéraires d'apprentissage
* apprentissage de l'autonomie dans un cadre sécurisant (aux limites clairement définies)
* favorise la motivation des étudiants -> moteur d'un fort investissement en temps et en énergie
* favorise également la reconnaissance et l'estime de soi pour les étudiants...

**Rôles de l'enseignant**

* **ingénieur pédagogique** qui conçoit le dispositif pédagogique (énoncé des objectifs, du cadre et des limites, du ou des thème(s), ...)
* **accompagnement pédagogique** : il organise les apprentissages nécessaires pour réaliser le projet (méthodologie, outils variés, ...)
* il est également le **médiateur** pour assurer le bon fonctionnement du groupe, ...
* Mais encore : ...

---

## Positionnement sur la Pédagogie de Projet

* une réalisation concrète => OUI (production d'un logiciel avec plusieurs interfaces : texte, graphique, android, web)
* intégrer des savoirs / compétences nouveaux => OUI (le projet nous sert de
  "prétexe" pour apprendre le *socle technique* en "codant / jouant")
* collectif / coopératif => 75% (mais garder des évaluations individuelles pour ...)
* choix libre du projet => NON (projet imposé, puzzle logique 2d, qui change chaque année)
* autonomie : choix libre des solutions au projet => 75% (implémentation libre MAIS dans un cadre imposé, assez contraint)
* motivation => FORTE (feedback rapide avec VPL + challenge pour maintenir la motivation dans le temps, mais risque de décrochage élevé -> stat des ghost/commit ?) 
* satisfaction des étudiants => FORTE (sondage anonyme depuis plusieurs années)
* auto-évaluation / bilan / restitution : BOF (évaluation par les pairs à mi-parcours, rapport de synthèse final, ...) 
* à compléter ...

**Nota Bene** : Présenter ce slide avec des curseurs de 0 à 100%...  comme les CVs étudiants !

**Discours** : Mon intuition est que l'on peut régler/adapter tous les curseurs
en fonction des objectifs pédagogiques (Lycée / Licence / Master, imposer ou non
un socle technique, taux d'encadrement, ...)


=> Quel est notre originalité et  nos spécificités ? 

---

## Slide de Présentation

"Projet en Licence : pourquoi, comment ?"
Aurélien Esnard et Nicolas Bonichon U-Bordeaux
TODO: trouver une illustrattion qui claque

blabla:

- Pour illustrer l'utilité des projets en licence, nous allons vous présenter notre retour d'expérience sur l'UE "Projet en Licence" de l'université de Bordeaux, dans laquelle nous sommes impliqué depuis 10 ans.
- Pour nous c'est un peu notre UE "pilote" dans laquelle on expérimente des choses, et on aimerait vous en faire profiter.
- On ne dit pas qu'on a la recette magique, mais nous pensons que vous arriverez à en tirer des idées pour vos propres enseignements.

---

## Slide Contexte Objectifs pédagogiques

### Contexte Licence L2 info effectif ...

- ils ont déjà fait du C
- UE qui s'étalle sur l'année.
- quelques cours + 1h20 par semaine de TP machine (feuille de TD ou suivi)

### Objectifs pédagogiques:

- Apprendre à travailler en équipe
  - équipes de 3
- débug du génie log
  - code propre
  - code testé
  - stratégie de débugage
  - découpage modulaire
  - un peu d'algorithmique
- Apprendre à utiliser des outils de dev
  - build : gcc / make / cmake
  - git
  - vscode / gdb / valgrind / doxygen / clang-format / kcachegrind / LaTeX
- sujet/prétexte : "coder un jeu casse tête"
  - même sujet pour tout le monde, mais sujet différent chaque année, mais on suit toujours le même schéma.

TODO: illustration du jeu jusqu'à la version android/web

---

## Pourquoi passer par un projet ?

- travail en équipe : OK, mais pas que !

- atteindre une taille critique pour que l'utilisation des outils et des méthodes soient utiles, voire indispensable

- Sur une activité de programmation courte (<= 1 semaine, <= 100 lignes de code, tout seul, ...), ces méthodes ne sont pas utiles voire contre-productives.
  - le quick and dirty est souvent plus efficace et c'est l'expérience qu'ils en font.
  - pourquoi apprendr gdb, alors qu'en rajoutant un printf on trouve le bug en 5 minutes ?
  - pourquoi donner aux variables des nom explicites, alors que j'ai tout en tête et qu'avec des noms en 1 lettre je fais moins de fautes de frappe dans mon éditeur rudimentaire ?
  - pourquoi m'embèter à faire un Makefile parce que mon code tient dans un fichier ?
  - ...

---

## Projet long =>  évaluation continue

- feedback rapide => plus efficace

bla bla bla:
Comme le projet est long, les étudiants ont besoin d'avoir des évaluations régulières pour savoir où ils en sont.
Mais cela représente beaucoup de travail pour l'équipe pédagogique.
Donc il faut mettre en place des outils pour automatiser le plus possible les évaluations.
Nous nous appuyons largement sur l'activité VPL de moodle comme nous allons le détailler.

Du coup séquencage :
-cours/ présentiel ou vidéo
-TP machine
-mise en application sur le projet (rendu évaluer)

TODO: illustration plan du cours
TODO: exemple de TD (cf. cours gitlab)
TODO: illustration VPL (1ère passe + 2ième passe)

---

## Problèmatiques et solutions pour l'évaluation automatique

Points importants à présenter :
problématiques :
- les étudiants s'appuient trop sur nos tests et de vérifie pas leur travail en amont
- l'étudiant qui n'a pas tout à fait respecté les consignes et qui se retrouve avec 0 alors que son travail ne mérite pas ça.
évaluation automatique en 3 temps !
-**avant la deadline** : accès à une partie de l'évaluation automatique (une partie de ce qui est demandée).
  - ça guide les étudiants qui sont un peu perdu.
  - cela reporte la tâche de "faire rentrer le rendu dans les consignes" de l'enseignant à l'étudiant. (libère du temps enseignant et forme les étudiants à respecter les consignes/le cahier des charges)
**jour de la deadline** : évaluation totale de tous les rendus avec des retours détaillés
**après la deadline** : "la deuxième passe". Les étudiants ont 1 semaine, pour améliorer leur note en soumettant un 2ième rendu.
  - comme les compétences s'empiles, les étudiants qui ont échoué peuvent rattraper leur retard.

### problématique :
-même sujet pour tout le monde : plagiat
 => outils de détection + historique gitlab/moodle + pour autopsier le plagiat

### problématique de l'utilisation d'outils auto...

Certains étudiants travaillent trop sur le projet et ça peut devenir contre-productif
Avant la deadline des rendus était le dimanche minuit.
Nous l'avons déplacé au vendredi 20h.

---

## Encore des trucs à dire (en vrac)

* alternance rendu préliminaire / final
* alternance rendu individuel & en équipe
* feedback rapides et réguliers (à chaque jalon) -> plutôt qu'un gros projet avec un seul rendu final
* solveur : approche à deux niveau 1) compétence de base du solveur brute-force et 2) challenge du meilleur solveur (optimisation) ...
* une UE qui leur demande bcp de travail, mais qui leur apporte bcp de compétences  -> évaluation de l'UE très positif depuis de nombreuses années...
* les étudiants sont fiers du chemin parcourus avec un projet qu'ils peuvent montrer à leur famille (SDL, Android, Web)
* classe inversée : mise à disposition des ressources à l'avance, ...
* communication enseignant-équipe: stop mail -> forum Moodle, puis Discord / RocketChat depuis la crise Sanitaire (bilan moyen cette année)
* utilisation de Moodle + GitLab pour les rendus... ()
* Quel compromis ? choix libre du projet VS entièrement spécifié -> evaluation automatique de programme sans regarder le détail de l'implémentation
* les difficultés rencontrés, .... (favoriser l'entraide)
  * encore chronophage pour l'équipe pédagogique : évaluation croisé, etc...
  * la gestion des équipes (pas toujours facile, plusieurs stratégies expérimentés) -> 0 commits => 0 à la note, note d'assiduité ?


* Pourquoi on est content :
  * les étudiants travaillent plus
  * les étudiants sont globalement contents
* Ce qu'il faut améliorer :
  * ça nous prend encore beaucoup d'énergie

---

## Annexes

* [Enseigner avec GitLab, 2022.](https://docs.google.com/presentation/d/1gCutRJcN_j9eSvSoCx-57IFw35LCoFSqXjXOHOTLf7g/edit?usp=sharing)
* [Version HAL](https://inria.hal.science/hal-04096066/document)
* [Vidéo] : https://mediapod.u-bordeaux.fr/video/39085-retour-dexperience-sur-une-ue-projet-en-licence-informatique-journee-enseignement-sif-2023/

---

## Organisation de la Journée

Objectifs de la journée :

* montrer des exemples qui fonctionnent
* faire réfléchir sur les intentions / objectifs
* transmettre quelques codes / éléments de langage
* donner envie d'approfondir

Organisation des exposés :

* Elie M. : 40-45 minutes + 15-20 minutes de questions
* Autres : 20 minutes + 5 minutes de questions de clarification pauses pour discuter

Organisation de la table ronde : 

* 1h30 prévue (en réalité 1h + dernières discussions). 
* Questions posées par l'animateur (liste fournie non limitative) + questions du public

Récupération des diaporamas :

* déposer un pdf au plus tard la veille mardi 9 à 10h ici : <https://seafile.unistra.fr/d/55ae2f3eb485414ba65c/>

---

## Table Ronde

Voici des suggestions de thèmes que vous pouvez aborder dans vos exposés :

* Quels étaient les objectifs d'apprentissage poursuivis ?
* Analyse des effets : quel est votre diagnostique sur l'atteinte des objectifs poursuivis ?
* Quelles ressources sont mises à disposition des étudiants ?
* Comment est organisé le travail des élèves / étudiants ou des groupes ?
* Quelles sont les modalités d'évaluation ?
* Quelle est la posture des enseignants ou tuteurs pour les projets ? Est-elle différente en comparaison aux méthodes pédagogiques transmissives ?

---
